angular.module('models', [])
.factory('$model', function() {
  var init = {
    Destination: function(obj){
      	this.destinationName = obj.city;
      	this.destinationImage = obj.image;
    },
    Trip: function(obj){
    	this.tripId = obj.tripid;
    	this.tripImage = obj.tripimage;
    	this.tripName = obj.tripname;
    	this.destination = obj.destination;
    	this.startdate = obj.startdate;
    	this.enddate = obj.enddate;
    },
    TripHotel: function(obj){
    	this.tripId = obj.tripid;
    	this.hotelName = obj.hotelname;
    	this.location = obj.location;
    	this.checkindate = obj.checkindate;
    	this.checkoutdate = obj.checkoutdate;
    },
    Hotel: function(obj){
    	this.hotelName = obj.hotelname;
    	this.availability = obj.availability;
    }
  };

  return init;
});