angular.module('service.trip', [])
.service('TripService', function($http) {

	var getDestination = function(){
		var dataUrl = "http://ionicdaysg.getsandbox.com/destination";
		return $http({
			method: 'GET',
			url: dataUrl,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallback(response) {
			if(response.data.meta.code == "200"){
				return response.data.data;
			}else{
				return null;
			}
		}, function errorCallback(response) {
			return null;
		});
	};

	var getHotel = function(searchtxt){
		var dataUrl = "http://ionicdaysg.getsandbox.com/hotels?term" + searchtxt;
		return $http({
			method: 'GET',
			url: dataUrl,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallback(response) {
			if(response.data.meta.code == "200"){
				return response.data.data;
			}else{
				return null;
			}
		}, function errorCallback(response) {
			return null;
		});
	};

	var doPostTrip = function(destination, name, startdate, enddate){
		var dataUrl = "http://ionicdaysg.getsandbox.com/trips";
		var datas = {"destination":destination,"tripname":name, "startdate":startdate, "enddate":enddate};
		return $http({
			method: 'POST',
			url: dataUrl,
			data: datas,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallBack(response){
			if(response.data.meta.code == "200"){
				return true;
			}else{
				return false;
			}
		}, function errorCallBack(response){
			return null;
		});
	};

	var doPostTripHotel = function(tripid, location, name, checkindate, checkoutdate){
		var dataUrl = "http://ionicdaysg.getsandbox.com/trips?tripid=" + tripid;
		var datas = {"hotelname":name, "location":location, "checkindate":checkindate, "checkoutdate":checkoutdate};
		return $http({
			method: 'POST',
			url: dataUrl,
			data: datas,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallBack(response){
			if(response.data.meta.code == "200"){
				return true;
			}else{
				return false;
			}
		}, function errorCallBack(response){
			return null;
		});
	};

	var getTrip = function(){
		var dataurl = "http://ionicdaysg.getsandbox.com/trips";

		return $http({
			method: 'GET',
			url: dataurl,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallBack(response){
			if(response.data.meta.code == "200"){
				return response.data.data;
			}else{	
				return null;
			}
		}, function errorCallBack(response){
			return null;
		});
	}

	var getTripHotel = function(tripid){
		var dataurl = "http://ionicdaysg.getsandbox.com/trips/hotel/" + tripid;

		return $http({
			method: 'GET',
			url: dataurl,
			headers: {
				'Content-Type': "application/json"
			},
		}).then(function successCallBack(response){
			if(response.data.meta.code == "200"){
				return response.data.data;
			}else{	
				return null;
			}
		}, function errorCallBack(response){
			return null;
		});
	}

	return {
		loadDestination: getDestination,
		loadHotel: getHotel,
		postTrip: doPostTrip,
		loadTrip: getTrip,
		postTripHotel: doPostTripHotel,
		loadTripHotel: getTripHotel
	}
});