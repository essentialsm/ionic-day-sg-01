angular.module('starter.account', ['shared.login'])
.controller('AccountCtrl', function($scope, $ionicPlatform, $localstorage, $cordovaImagePicker, ModalLogin) {

	$scope.fullname = "";
	$scope.profilepic = "img/profile-ph.png"

	$ionicPlatform.ready(function(){});

	$scope.getImageGallery = function() {      
      var options = {
          maximumImagesCount: 1,
          width: 600,
          height: 600,
          quality: 80
      };

      $cordovaImagePicker.getPictures(options).then(function (results) {
          if(results.length > 0){
            console.log('Image URI: ' + results[0]);
            $scope.profilepic = results[0];

            //upload to server
          }
      }, function(error) {
          console.log('Error: ' + JSON.stringify(error));
      });
    };

	$scope.$on('$ionicView.beforeEnter', function(){
		//check cache is login?
		if(!$localstorage.get("islogin")){
			//do something
		}else{
			//call login modal:
			ModalLogin.init($scope)
				.then(function(modal){
					modal.show();
				});
		}
	});

});