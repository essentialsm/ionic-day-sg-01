angular.module('shared.hotel', ['service.trip'])
.service('ModalHotel', function($ionicModal, $http, $rootScope, TripService, $model, ModalAddHotel) {

	var init = function($scope){
    var promise;
    $parentScope = $scope;
    $scope = $scope || $rootScope.$new();

    $scope.isworking = false;
    $scope.hotels = []; //init

    $scope.model = {
      searchText: ''
    };

    promise = $ionicModal.fromTemplateUrl('templates/shared/searchhotel.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      return modal;
    });

    $scope.onTextChanged = function(){
      //search contest here:
      $scope.isworking = true;
      $scope.hotels = []; //reset

      if($scope.model.searchText.trim() != ""){
        var promise = TripService.loadHotel($scope.model.searchText);
        promise.then(function(result) {
          if(result != null){
            for(var i = 0; i < result.length; i++){
              var hotel = new $model.Hotel(result[i]);
              $scope.hotels.push(hotel);
            }
          }

          $scope.isworking = false;
        });
      }
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.addHotel = function(hotelname) {
      ModalAddHotel.init($scope, $parentScope, hotelname).then(function(modal){
        modal.show();
      });
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    return promise;
  }

  return {
    init: init
  }

})
.service('ModalAddHotel', function($ionicModal, $http, $ionicPopup, $rootScope, TripService, $model, ionicDatePicker){

  var init = function($scope, $parentscope, hotelname){
    var promise;
    $mainModalScope = $scope;
    $parentScope = $parentscope;
    $scope = $rootScope.$new();

    $scope.isworking = false;
    $scope.hotels = []; //init

    $scope.model = {
      hotelName: hotelname,
      checkindate: '',
      checkoutdate: '',
      location: ''
    };

    promise = $ionicModal.fromTemplateUrl('templates/shared/addhotel.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      return modal;
    });

    $scope.back = function() {
      $scope.modal.hide();
    };

    var checkindate = {
      callback: function (val) {  //Mandatory
        var dt = new Date(val);
        $scope.model.checkindate = dt.getDate() + "-" + dt.getMonth() + "-" + dt.getFullYear();
      },
      dateFormat: 'dd-MM-yyyy'
    };

    var checkoutdate = {
      callback: function (val) {  //Mandatory
        var dt = new Date(val);
        $scope.model.checkoutdate = dt.getDate() + "-" + dt.getMonth() + "-" + dt.getFullYear();
      },
      dateFormat: 'dd-MM-yyyy'
    };

    $scope.submitHotelBooking = function() {
      var promise = TripService.postTripHotel($parentScope.tripID, $scope.model.location, $scope.model.hotelName, $scope.model.checkindate, $scope.model.checkoutdate);
      promise.then(function(result){
        if(result){

          //add new hotel to parentscope:
          var obj = {
            tripid:$parentScope.tripID, 
            hotelname:$scope.model.hotelName, 
            location:$scope.model.location, 
            checkindate:$scope.model.checkindate, 
            checkoutdate: $scope.model.checkoutdate
          }

          var newHotelTrip = new $model.TripHotel(obj);
          $parentScope.triphotels.push(newHotelTrip);
          $parentScope.hotelEmpty = false;

          //close modal here:
          $scope.modal.hide(); //my modal
          $mainModalScope.modal.hide(); //parent modal
        }else{
          $ionicPopup.alert({
             title: 'Trip',
             subTitle: 'Unable to add hotel to your trip!'
          });
        }
      });
    };

    $scope.pickCheckin = function(){
      ionicDatePicker.openDatePicker(checkindate);
    };

    $scope.pickCheckout = function(){
      ionicDatePicker.openDatePicker(checkoutdate);
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    return promise;
  }

  return {
    init: init
  }

});