angular.module('shared.login', [])
.service('ModalLogin', function($ionicModal, $state, $q, $http, $rootScope, $model, $ionicPopup, $localstorage) {

	var init = function($scope){
    var promise;
    $parentScope = $scope;
    $scope = $scope || $rootScope.$new();

    $scope.isworking = false;
    $scope.hotels = []; //init

    $scope.model = {
      searchText: ''
    };

    promise = $ionicModal.fromTemplateUrl('templates/shared/login.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      return modal;
    });

    $scope.closeModal = function() {
      $state.go("tab.trip");
      $scope.modal.hide();
    };

    $scope.fbLoginError = function(response){
      $ionicPopup.alert({
         title: 'Login failed',
         subTitle: 'Unable to login to Facebook',
      });
    }

    $scope.fbLoginSuccess = function(response){ 
      var authResponse = response.authResponse;
      $scope.getFacebookProfileInfo(authResponse)
      .then(function(profileInfo) {
        var picture = "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large";
        var name = profileInfo.name;
        
        //close modal goback:
        $parentScope.fullname = name;
        $localstorage.set("islogin", true);
        $scope.modal.hide();
      }, function(fail){
        // Fail get profile info
        console.log('profile info fail', fail);
        $ionicPopup.alert({
           title: 'Login failed',
           subTitle: 'Failed to get profile from Facebook, Please try again',
        });
      });
    }

    // This method is to get the user profile info from the facebook api
    $scope.getFacebookProfileInfo = function (authResponse) {
      var info = $q.defer();
      facebookConnectPlugin.api('/me?fields=email,id,name,gender,verified&access_token=' + authResponse.accessToken, null,
        function (response) {
          info.resolve(response);
        },
        function (response) {
          info.reject(response);
        }
      );
      return info.promise;
    };

    //loginFB:
    $scope.loginFB = function() {
      facebookConnectPlugin.getLoginStatus(function(success){
        console.log("banana " + success.status);
        if(success.status === 'connected'){
          //check local storage curen FB ID
          //var user = $localstorage.get(AUTH_EVENTS.CURRFBID);

          $scope.getFacebookProfileInfo(success.authResponse)
          .then(function(profileInfo) {
            var picture = "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large";
            var verified = profileInfo.verified ? 1:0;
            var name = profileInfo.name;

            $parentScope.fullname = name;
            $localstorage.set("islogin", true);
            $scope.modal.hide();
          }, function(fail){
            // Fail get profile info
            $ionicPopup.alert({
               title: 'Login failed',
               subTitle: 'Failed to get profile from Facebook, Please try again',
            });
          });
        }else{
          // Ask the permissions you need. You can learn more about
          // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
          facebookConnectPlugin.login(['email', 'public_profile'], $scope.fbLoginSuccess, $scope.fbLoginError);
        }
      });
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    return promise;
  }

  return {
    init: init
  }

});